from django.apps import AppConfig


class PageStatusConfig(AppConfig):
    name = 'page_status'
