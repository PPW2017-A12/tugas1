from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index, add_status
from .models import Status
from .forms import Add_Status_Form


# Create your tests here.
class PageStatusUnitTest(TestCase):
    def test_page_status_is_exist(self):
        response = Client().get('/status/')
        self.assertEqual(response.status_code, 200)

    def test_page_status_is_landing_page(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 301)
        self.assertRedirects(response, '/status/', 301, 200)

    def test_model_can_create_new_status(self):
        new_status = Status.objects.create(status="testing bikin status nih")
        counting_all_status = Status.objects.all().count()
        self.assertEqual(counting_all_status, 1)

    def test_page_status_post_success_and_render_the_status(self):
        new_status = "Masih mencoba bikin status"
        response_post = Client().post('/status/add_status', {'status': new_status})
        self.assertEqual(response_post.status_code, 302)

        response = Client().get('/status/')
        html_response = response.content.decode('utf8')
        self.assertIn(new_status, html_response)

    def test_page_status_post_failed_because_no_status(self):
        test = 'Anonymous'
        response_post = Client().post('/status/add_status', {'status': ''})
        self.assertEqual(response_post.status_code, 302)

        response = Client().get('/status/')
        html_response = response.content.decode('utf8')
        self.assertNotIn(test, html_response)
