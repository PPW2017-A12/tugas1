import json
from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import Add_Status_Form
from .models import Status
from page_profile.models import Account

# Create your views here.
response = {}


def index(request):
    status = Status.objects.all()
    status = status[::-1]
    response['author'] = "Kelompok 12 PPW-A"
    response['status'] = status
    html = "page_status/page_status.html"
    response['status_form'] = Add_Status_Form
    response['profile'] = Account.objects.first()
    response['profile'].expertises = json.loads(response['profile'].expertises)
    return render(request, html, response)


def add_status(request):
    form = Add_Status_Form(request.POST or None)
    if (request.method == 'POST' and form.is_valid()):
        response['status'] = request.POST['status']
        status = Status(status=response['status'])
        status.save()
        return HttpResponseRedirect('/status/')
    else:
        return HttpResponseRedirect('/status/')
