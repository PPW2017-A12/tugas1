from django import forms


class Add_Status_Form(forms.Form):
    error_meesages = {
        'required': 'Tolong isi input ini',
    }

    status_attrs = {
        'class': 'form-control',
    }

    status = forms.CharField(label='', widget=forms.Textarea(attrs=status_attrs), required=True)
