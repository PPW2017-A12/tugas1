import json
from django.shortcuts import render
from .models import Account


# Create your views here.
def index(request):
    response = {}
    html = 'page_profile/page_profile.html'

    response['profile'] = Account.objects.first()
    response['profile'].expertises = json.loads(response['profile'].expertises)

    return render(request, html, response)
