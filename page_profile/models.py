from django.db import models
from datetime import date
import json


class SingleAccountObject:
    name = 'Kak Pewe'
    avatar = 'img/picture.jpg'
    birthday = date(2017, 8, 28)
    gender = 'L'
    expertises = json.dumps(["programming", "helping"])
    description = 'Official Mascot of PPW 2017'
    email = 'kak.pewe@ui.ac.id'


# Create your models here.
class Account(models.Model):
    GENDER_CHOICES = (
        ('M', 'Male'),
        ('F', 'Female')
    )

    name = models.CharField(max_length=70)
    avatar = models.CharField(max_length=70)
    birthday = models.DateField()
    gender = models.CharField(max_length=1, choices=GENDER_CHOICES)
    expertises = models.CharField(max_length=200)
    description = models.TextField()
    email = models.EmailField()
