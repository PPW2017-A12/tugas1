from django.apps import apps
from django.test import TestCase
from django.test import Client
from django.db import connection
from django.db.migrations.executor import MigrationExecutor

from .models import *


# Create your tests here.
class ProfilePageUnitTest(TestCase):
    def test_page_profile_exist(self):
        response = Client().get('/profile/')
        self.assertEqual(response.status_code, 200)

    def test_page_showing_single_model_object_profile(self):
        response = Client().get('/profile/')
        html_response = response.content.decode('utf8')

        self.assertIn(SingleAccountObject.name, html_response)
        self.assertIn(SingleAccountObject.birthday.strftime("%b %-d, %-Y"), html_response)
        self.assertIn(SingleAccountObject.gender, html_response)
        self.assertIn(SingleAccountObject.description, html_response)
        self.assertIn(SingleAccountObject.email, html_response)

        for expertise in json.loads(SingleAccountObject.expertises):
            self.assertIn(expertise, html_response)

    def test_single_model_object(self):
        account = Account.objects.first()
        self.assertEqual(1, Account.objects.count())

        self.assertEqual(SingleAccountObject.name, account.name)
        self.assertEqual(SingleAccountObject.avatar, account.avatar)
        self.assertEqual(SingleAccountObject.birthday, account.birthday)
        self.assertEqual(SingleAccountObject.gender, account.gender)
        self.assertEqual(SingleAccountObject.expertises, account.expertises)
        self.assertEqual(SingleAccountObject.description, account.description)
        self.assertEqual(SingleAccountObject.email, account.email)

    def test_custom_migration_reverse_and_forward(self):
        initial_migration = [("page_profile", "0001_initial")]
        migration = [("page_profile", "0005_auto_20171013_0207")]

        # test reverse custom migration
        executor = MigrationExecutor(connection)
        executor.migrate(initial_migration)
        self.assertEqual(0, Account.objects.count())

        # test forward custom migration
        executor = MigrationExecutor(connection)
        executor.migrate(migration)
        self.test_single_model_object()
