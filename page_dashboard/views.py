from django.shortcuts import render
from page_friends.models import Add_Friend
from page_status.models import Status
from page_profile.models import Account, SingleAccountObject

# Create your views here.
response = {}
feed = Status.objects.all()
friends = Add_Friend.objects.all()
identity = Account.objects.all()


def index(request):
    response['identity'] = identity.get().name
    response['picture'] = identity.get().avatar
    if (feed != None):
        response['latest'] = feed.last()
    response['feed'] = feed.count()
    response['friends'] = friends.count()
    html = "page_dashboard/page_dashboard.html"
    return render(request, html, response)
