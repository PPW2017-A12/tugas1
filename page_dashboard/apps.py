from django.apps import AppConfig


class PageDashboardConfig(AppConfig):
    name = 'page_dashboard'
