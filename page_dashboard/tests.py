from django.test import TestCase
from django.test import Client
from django.urls import resolve
from page_status.models import Status
from page_friends.models import Add_Friend
from .views import index, feed, friends, identity


# Create your tests here.
class PageDashboardUnitTest(TestCase):
    def test_page_dashboard_is_exist(self):
        response = Client().get('/dashboard/')
        self.assertEqual(response.status_code, 200)

    def test_page_dashboard_is_using_index_func(self):
        found = resolve('/dashboard/')
        self.assertEqual(found.func, index)

    def test_page_dashboard_get_feeds_from_page_status(self):
        get_feeds = Status.objects.all().count()
        self.assertEqual(get_feeds, feed.count())

    def test_page_dashboard_get_latest_post_from_page_status(self):
        get_latest_post = Status.objects.all().last()
        self.assertEqual(get_latest_post, feed.last())

    def test_page_dashboard_get_friends_from_page_friends(self):
        get_friends = Add_Friend.objects.all().count()
        self.assertEqual(get_friends, friends.count())
