## PPW-A Kelompok 12

## Team Member
- Alsabila Shakina Prasetyo
- Justin
- Kristianto
- Muhammad Ashlah Shinfain

## Pipeline status
[![pipeline status](https://gitlab.com/PPW2017-A12/tugas1/badges/master/pipeline.svg)](https://gitlab.com/PPW2017-A12/tugas1/commits/master)

## Coverage report
[![coverage report](https://gitlab.com/PPW2017-A12/tugas1/badges/master/coverage.svg)](https://gitlab.com/PPW2017-A12/tugas1/commits/master)

## Link Heroku App
https://ppw-a12-tugas1.herokuapp.com/