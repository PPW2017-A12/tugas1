from django.conf.urls import url
from .views import index, add_friend_post

urlpatterns = [
    url(r'^$', index, name="index"),
    url(r'^add_friend', add_friend_post, name='add_friend_post'),
]
