from django.db import models
import datetime


# Create your models here.
class Add_Friend(models.Model):
    name = models.CharField(max_length=27)
    url = models.URLField()
    date = models.DateTimeField(auto_now_add=True)
