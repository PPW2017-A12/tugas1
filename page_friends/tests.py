from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from .views import index, add_friend_post
from .models import Add_Friend
from .forms import Add_Friend_Form


# Create your tests here.
class Lab4UnitTest(TestCase):
    def test_page_friends_is_exist(self):
        response = Client().get('/friends/')
        self.assertEqual(response.status_code, 200)

    def test_page_friends_using_index_func(self):
        found = resolve('/friends/')
        self.assertEqual(found.func, index)

    def test_model_can_add_friend(self):
        new_friend = Add_Friend.objects.create(name='test', url='justin-tutorial0.herokuapp.com')

        counting_all_available_friend = Add_Friend.objects.all().count()
        self.assertEqual(counting_all_available_friend, 1)

    def test_page_friends_post_success_and_render_the_result(self):
        response_post = Client().post('/friends/add_friend',
                                      {'name': 'dummy', 'url': 'http://dummy.herokuapp.com'})
        self.assertEqual(response_post.status_code, 302)

        response = Client().get('/friends/')
        html_response = response.content.decode('utf8')
        self.assertIn('dummy', html_response)
        self.assertIn('http://dummy.herokuapp.com', html_response)

    def test_page_friends_post_fail(self):
        response = Client().post('/friends/add_friend', {'name': 'Anonymous', 'url': 'netnot'})
        self.assertEqual(response.status_code, 302)
