from django import forms


class Add_Friend_Form(forms.Form):
    error_messages = {
        'required': 'Please fill this',
    }
    attrs = {
        'class': 'form-control'
    }
    name = forms.CharField(label='Name', required=True, max_length=27, widget=forms.TextInput(attrs=attrs))
    url = forms.URLField(label='Url', required=True, widget=forms.URLInput(attrs=attrs))
