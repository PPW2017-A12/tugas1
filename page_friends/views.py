from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import Add_Friend_Form
from .models import Add_Friend

# Create your views here.
response = {}


def index(request):
    response['add_friend_form'] = Add_Friend_Form
    response['friends'] = Add_Friend.objects.all()
    html = 'page_friends/page_friends.html'
    return render(request, html, response)


def add_friend_post(request):
    form = Add_Friend_Form(request.POST or None)
    if (request.method == 'POST' and form.is_valid()):
        response['name'] = request.POST['name']
        response['url'] = request.POST['url']
        add_friend = Add_Friend(name=response['name'], url=response['url'])
        add_friend.save()
        return HttpResponseRedirect('/friends/')
    else:
        return HttpResponseRedirect('/friends/')
