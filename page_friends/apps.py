from django.apps import AppConfig


class PageFriendsConfig(AppConfig):
    name = 'page_friends'
